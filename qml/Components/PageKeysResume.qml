/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import QtQuick.LocalStorage 2.0
//import "../js/otpauth.min.js" as OTPAuth
import "../js/db.js" as KeysDB

Page {
    anchors.fill: parent

    signal timerTicked(int time)
    property bool isListBusy: false

    //TODO: Do we want to show the hint only when the Flickable is at the top?
    //property alias isAtTop: keysListView.atYBeginning

    header: HeaderMain {
        id: pageHeader
        title: i18n.tr('2FA Manager')
        flickable: keysListView
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: keysListView
    }

    ListView {
        id: keysListView
        anchors.fill: parent
        model: storedKeys
        delegate: keysDelegate
    }

    ListModel {
        id: storedKeys

        function populate(data) {
            storedKeys.clear();
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var parsedKey = OTPAuth.URI.parse(data.item(i).keyJSON);
                    storedKeys.append({
                        desc: data.item(i).description,
                        authkey: parsedKey,
                        secret: parsedKey.secret.b32,
                        //We use id to delete the proper db entry
                        id: data.item(i).identifier,
                        url: data.item(i).keyJSON
                    });
                }
            }
        }
    }

    Loader {
        id: emptyStateLoader
        anchors.fill: parent
        active: storedKeys.count === 0 && !isListBusy
        source: Qt.resolvedUrl("EmptyDocument.qml")
    }

    Component.onCompleted: {
        initialize();

        //TODO: Remove after debugging
        //renderOTPuri("otpauth://totp/otplib:demo?digits=8&secret=IE2C6Y3MMVHHGQ3TGN3GOWTKJVJXG6BZ&issuer=otplib")
    }

    Connections {
        target: root

        onInitDB: {
            console.log('Received initDB signal');
            initialize();
        }
    }

    function initialize() {
        isListBusy = true;
        var allKeys = KeysDB.getKeys();
        storedKeys.populate(allKeys.rows);
        isListBusy = false;
    }
}
